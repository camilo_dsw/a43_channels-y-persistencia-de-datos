

    @if ($errors->any())
    <h1>Error al introducir un Link</h1>
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form-group">
        <label for="Channel">Channel:</label>
            <select class="form-control @error('channel_id') is-invalid @enderror" name="channel_id">
                <option selected disabled >Pick a Channel...</option>
                    @foreach ($channels  as $channel)
                    <option value="{{ $channel->id }}">
                        {{ $channel->title }}
                        </option>
                    @endforeach
            </select>
        @error('channel_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
