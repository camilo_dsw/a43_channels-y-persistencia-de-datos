<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Models\CommunityLink;
use App\Models\Channel;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channel $channel = null)
    {
        //
        //return view('community/index');

        // para pasarle a la vista todos los canales y poder 
        //mostrarlos ordenados en el formulario:
       $channels = Channel::orderBy('title','asc')->get();
      
       // para pasarle a la vista todos los links y poder 
        //mostrarlos ordenados en el formulario:
       $links = CommunityLink::paginate(10);
       return view('community.index', compact('links','channels', 'channel'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('community/index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
            'title' => 'required',
            'link' => 'required|active_url|unique:community_links',
            'channel_id' => 'required|exists:channels,id'

          ]);

        request()->merge(['user_id' => Auth::id()]);
        CommunityLink::create($request->all());
                
        return back();
        //'required|exists:channels,id'
        //'user_id' => Auth::id()
        //'channel_id' =>  'required|exists:channels, id',
        //'link' => 'required|active_url|unique:community_links'
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {
        //
    }

    
}
